terraform {
  required_version = ">= 0.12, < 0.13"
}

resource "aws_db_instance" "example" {
  identifier_prefix = "terraform-up-and-running"
  engine            = "mysql"
  allocated_storage = 10
  instance_class    = var.db_instance_type

  username = "admin"

  name                = var.db_name
  skip_final_snapshot = true

  password = var.db_password
}
